import os
basedir = os.path.abspath(os.path.dirname(__file__))

DB_NAME = 'app_test.db'
DB_FULL_FILE_PATH = os.path.join(basedir, DB_NAME)
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DB_FULL_FILE_PATH
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository_test')
SQLALCHEMY_TRACK_MODIFICATIONS = False