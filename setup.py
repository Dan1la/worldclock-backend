from flask import g
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import app.app
from app.db_base.base_sqlalchemy import ForeignKeysListener


def get_db():
    with app.app.server.app_context():
        db = getattr(g, '_db_session', None)
        if db is None:
            engine = create_engine(app.app.server.config.get('SQLALCHEMY_DATABASE_URI'), listeners=[ForeignKeysListener()], echo=False)
            Session = sessionmaker(bind=engine)
            Session.configure(bind=engine)
            db = g._db_session = Session()
        return db