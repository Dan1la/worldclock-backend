#!venv/bin/python
import pytz
from migrate.versioning import api

from app.domain.role import Role
from app.repositories.user_role_sql_repository import UserRoleSqlRepository
from app.repositories.user_sql_repository import UserSqlRepository
from app.repositories.user_timezone_sql_repository import UserTimezoneSqlRepository
from app.services.user_account_service import UserAccountService
from app.services.user_role_service import UserRoleService
from app.services.user_timezone_service import UserTimezoneService
from config import SQLALCHEMY_DATABASE_URI
from config import SQLALCHEMY_MIGRATE_REPO
import os.path
from sqlalchemy import create_engine
from app.db_base.base_sqlalchemy import Base, ForeignKeysListener
from setup import get_db
from jsonschema import validate

engine = create_engine(SQLALCHEMY_DATABASE_URI, listeners=[ForeignKeysListener()], echo=False)
Base.metadata.create_all(engine)
session = get_db()

user_role_service = UserRoleService(UserRoleSqlRepository(session))
user_role_service.add_role(Role.USER.value, Role.USER.name)
user_role_service.add_role(Role.USER_MANAGER.value, Role.USER_MANAGER.name)
user_role_service.add_role(Role.ADMIN.value, Role.ADMIN.name)

user_timezone_service = UserTimezoneService(UserTimezoneSqlRepository(session))
for timezone in pytz.all_timezones:
    user_timezone_service.add_timezone(timezone)

account_service = UserAccountService(UserSqlRepository(session))
account_service.sign_up('Daniil', 'Archipovd', 'admin@example.com', 'Pa55word', Role.ADMIN.value)

if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
    api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
    api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
else:
    api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, api.version(SQLALCHEMY_MIGRATE_REPO))
