import pytest
import pytz
import random

from sqlalchemy.orm.exc import NoResultFound

from test_utils import generate_and_register_random_user


def test_add_timezone(user_timezone_service):
    random_timezone_name = random.choice(pytz.all_timezones)
    timezone_id = user_timezone_service.add_timezone(random_timezone_name)
    timezone = user_timezone_service.get_timezone(timezone_id)
    assert timezone.timezone_id == timezone_id and timezone.name == random_timezone_name


def test_add_timezone_and_get_by_name(user_timezone_service):
    random_timezone_name = random.choice(pytz.all_timezones)
    user_timezone_service.add_timezone(random_timezone_name)
    timezone = user_timezone_service.get_timezone_by_name(random_timezone_name)
    assert timezone.name == random_timezone_name


def test_delete_timezone(user_timezone_service):
    with pytest.raises(NoResultFound):
        random_timezone_name = random.choice(pytz.all_timezones)
        timezone_id = user_timezone_service.add_timezone(random_timezone_name)
        user_timezone_service.delete_timezone(timezone_id)
        user_timezone_service.get_timezone(timezone_id)

def test_add_user_timezone(account_service, user_timezone_service):
    u = generate_and_register_random_user(account_service)
    timezone = random.choice(pytz.all_timezones)
    timezone_id = user_timezone_service.add_timezone(timezone)
    user_timezone_service.add_user_timezone(u.user_id, timezone_id)
    timezones = account_service.get_user(u.user_id).timezones
    assert timezone == timezones[0].timezone.name


def test_add_user_timezone2(account_service, user_timezone_service):
    u = generate_and_register_random_user(account_service)
    random_timezone = random.choice(pytz.all_timezones)
    timezone_id = user_timezone_service.add_timezone(random_timezone)
    user_timezone_service.add_user_timezone(u.user_id, timezone_id)

    random_timezone2 = random.choice(pytz.all_timezones)
    timezone_id = user_timezone_service.add_timezone(random_timezone2)
    user_timezone_service.add_user_timezone(u.user_id, timezone_id)

    timezones = account_service.get_user(u.user_id).timezones
    assert random_timezone == timezones[0].timezone.name
    assert random_timezone2 == timezones[1].timezone.name


def test_add_delete_user_timezones(account_service, user_timezone_service):
    u = generate_and_register_random_user(account_service)

    user_timezone_service.add_user_timezone_by_name(u.user_id, pytz.all_timezones[0])
    user_timezone_service.add_user_timezone_by_name(u.user_id, pytz.all_timezones[1])

    timezones = account_service.get_user(u.user_id).timezones
    assert pytz.all_timezones[0] == timezones[0].timezone.name
    assert pytz.all_timezones[1] == timezones[1].timezone.name

    user_timezone_service.delete_user_timezone(u.user_id, timezones[1].timezone_id)
    timezones = account_service.get_user(u.user_id).timezones

    assert len(timezones) == 1
    assert timezones[0].timezone.name == pytz.all_timezones[0]

    user_timezone_service.delete_user_timezone(u.user_id, timezones[0].timezone_id)
    timezones = account_service.get_user(u.user_id).timezones

    assert len(timezones) == 0


def test_add_delete_user_timezones2(account_service, user_timezone_service):
    u = generate_and_register_random_user(account_service)

    user_timezone_service.add_user_timezone_by_name(u.user_id, pytz.all_timezones[0])
    user_timezone_service.add_user_timezone_by_name(u.user_id, pytz.all_timezones[1])

    u2 = generate_and_register_random_user(account_service)
    user_timezone_service.add_user_timezone_by_name(u2.user_id, pytz.all_timezones[0])
    user_timezone_service.add_user_timezone_by_name(u2.user_id, pytz.all_timezones[1])

    timezones = account_service.get_user(u.user_id).timezones
    assert pytz.all_timezones[0] == timezones[0].timezone.name
    assert pytz.all_timezones[1] == timezones[1].timezone.name

    user_timezone_service.delete_user_timezone(u.user_id, timezones[1].timezone_id)
    timezones = account_service.get_user(u.user_id).timezones

    assert len(timezones) == 1
    assert timezones[0].timezone.name == pytz.all_timezones[0]

    u2_timezones = account_service.get_user(u2.user_id).timezones
    assert pytz.all_timezones[0] == u2_timezones[0].timezone.name
    assert pytz.all_timezones[1] == u2_timezones[1].timezone.name

    user_timezone_service.delete_user_timezone(u.user_id, timezones[0].timezone_id)
    timezones = account_service.get_user(u.user_id).timezones

    assert len(timezones) == 0

    user_timezone_service.delete_user_timezone(u2.user_id, u2_timezones[0].timezone_id)
    user_timezone_service.delete_user_timezone(u2.user_id, u2_timezones[1].timezone_id)
    u2_timezones = account_service.get_user(u2.user_id).timezones
    assert len(u2_timezones) == 0


