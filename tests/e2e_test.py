import httplib
from base64 import b64encode

import pytz
from flask import json

from app.domain.role import Role


def test_missing_credentials_header(flask_test_server):
    r = flask_test_server.get('/account/me')
    assert r.status_code == httplib.UNAUTHORIZED


def test_sign_up(flask_test_server):
    first_name = 'Daniil'
    last_name = 'Archipov'
    username = 'test@example.com'
    password = '123123'
    r = sign_up(flask_test_server, first_name, last_name, username, password)
    assert r.status_code == httplib.OK
    r2 = flask_test_server.get('/account/me', headers=generate_basic_auth_header(username, password))
    data = json.loads(r2.data)
    assert data['email'] == username and data['first_name'] == first_name and data['last_name'] == last_name \
           and data['role'] == Role.USER.name


def test_sign_up_user_twice_fail(flask_test_server):
    r = sign_up(flask_test_server, "AAA", "BBB", "email@email.com", "pa55word")
    assert r.status_code == httplib.OK
    r2 = sign_up(flask_test_server, "AAA", "BBB", "email@email.com", "pa55word")
    assert r2.status_code == httplib.CONFLICT


def test_role_incorrect(flask_test_server):
    username = 'test@example.com'
    password = '123123'
    sign_up(flask_test_server, "Daniil", "Archipov", username, password)
    r2 = flask_test_server.get('/account/user/all', headers=generate_basic_auth_header(username, password))
    assert r2.status_code == httplib.FORBIDDEN


def test_update_user_role(flask_test_server):
    admin_user_name = 'admin@example.com'
    admin_password = 'Pa55word'

    user_username = 'random@email.com'
    user_password = 'hounds123'
    first_name = "Peter"
    last_name = 'Houndhunter'

    sign_up(flask_test_server, first_name, last_name, user_username, user_password)
    new_role = Role.USER_MANAGER.name
    update_response = update_role_for_user(flask_test_server, user_username, new_role, admin_user_name, admin_password)
    assert update_response.status_code == httplib.OK

    get_user_response = get_user_by_username(flask_test_server, user_username, admin_user_name, admin_password)

    data = json.loads(get_user_response.data)
    assert data['first_name'] == 'Peter' and data['last_name'] == 'Houndhunter' and data['email'] == user_username and \
           data['role'] == new_role


def test_update_user_details_permissions_ok(flask_test_server):
    username = 'test123@example.com'
    password = '123123'
    r = sign_up(flask_test_server, "Daniil", 'Archipov', username, password)
    assert r.status_code == httplib.OK

    new_first_name = 'John'
    new_last_name = 'Smith'
    new_email = 'john@yahoo.com'
    new_password = 'Pa55word'

    r = update_user_details(flask_test_server, username, new_first_name, new_last_name, new_email, new_password,
                            username, password)
    assert r.status_code == httplib.OK

    r2 = get_me(flask_test_server, new_email, new_password)
    data = json.loads(r2.data)
    assert data['email'] == new_email and data['first_name'] == new_first_name and data['last_name'] == new_last_name


def test_update_user_details_permissions_invalid(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'

    user_username2 = 'random2@email.com'
    user_password2 = 'hounds123'

    r1 = sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    r2 = sign_up(flask_test_server, "Daniil2", 'Archipov2', user_username2, user_password2)

    r3 = update_user_details(flask_test_server, user_username2, "Random", "Random", "Random", "Random", user_username1,
                             user_password1)
    assert r3.status_code == httplib.FORBIDDEN


def test_update_user_details_manager_updates_ok(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'

    user_username2 = 'random2@email.com'
    user_password2 = 'hounds123'

    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    sign_up(flask_test_server, "Daniil2", 'Archipov2', user_username2, user_password2)

    new_role = Role.USER_MANAGER.name
    admin_user_name = 'admin@example.com'
    admin_password = 'Pa55word'
    r = update_role_for_user(flask_test_server, user_username1, new_role, admin_user_name, admin_password)

    r3 = update_user_details(flask_test_server, user_username2, "Random", "Random", "Random", "Random", user_username1,
                             user_password1)
    assert r3.status_code == httplib.OK


def test_update_user_details_manages_updates_admin_fail(flask_test_server):
    admin_user_name = 'admin@example.com'
    admin_password = 'Pa55word'

    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'

    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    r = update_role_for_user(flask_test_server, user_username1, Role.USER_MANAGER.name, admin_user_name, admin_password)

    r3 = update_user_details(flask_test_server, admin_user_name, "Random", "Random", "Random", "Random", user_username1,
                             user_password1)

    assert r3.status_code == httplib.FORBIDDEN


def test_delete_user_permission_ok(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)

    r1 = delete_user(flask_test_server, user_username1, user_username1, user_password1)
    assert r1.status_code == httplib.OK
    r2 = get_user_by_username(flask_test_server, user_username1)
    assert r2.status_code == httplib.NOT_FOUND


def test_delete_user_manager_deletes_manager_forbidden(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    update_role_for_user(flask_test_server, user_username1, Role.USER_MANAGER.name)
    user_username2 = 'random2@email.com'
    user_password2 = 'hounds123'
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username2, user_password2)
    update_role_for_user(flask_test_server, user_username2, Role.USER_MANAGER.name)

    r1 = delete_user(flask_test_server, user_username2, user_username1, user_password1)
    assert r1.status_code == httplib.FORBIDDEN


def test_delete_user_user_deletes_manager_forbidden(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    update_role_for_user(flask_test_server, user_username1, Role.USER_MANAGER.name)
    user_username2 = 'random2@email.com'
    user_password2 = 'hounds123'
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username2, user_password2)
    update_role_for_user(flask_test_server, user_username2, Role.ADMIN.name)

    r1 = delete_user(flask_test_server, user_username2, user_username1, user_password1)
    assert r1.status_code == httplib.FORBIDDEN


def test_add_timezone_to_user(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    r1 = add_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[0], user_username1, user_password1)
    r2 = add_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[1], user_username1, user_password1)
    assert r1.status_code == httplib.OK and r2.status_code == httplib.OK

    user = get_me(flask_test_server, user_username1, user_password1)
    data = json.loads(user.data)
    assert data['timezones'][0]['timezone_name'] == pytz.all_timezones[0] and data['timezones'][1]['timezone_name'] == \
                                                                              pytz.all_timezones[1]


def test_add_timezone_to_user_manager_add_permission_ok(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'

    user_username2 = 'random2@email.com'
    user_password2 = 'hounds123'

    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username2, user_password2)
    update_role_for_user(flask_test_server, user_username2, Role.USER_MANAGER.name)
    r2 = add_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[1], user_username2, user_password2)
    assert r2.status_code == httplib.OK

    user = get_user_by_username(flask_test_server, user_username1, user_username2, user_password2)
    data = json.loads(user.data)
    assert data['timezones'][0]['timezone_name'] == pytz.all_timezones[1]


def test_add_timezone_to_user_permission_failed(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'

    user_username2 = 'random2@email.com'
    user_password2 = 'hounds123'

    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username2, user_password2)

    r2 = add_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[1], user_username2, user_password2)
    assert r2.status_code == httplib.FORBIDDEN


def test_delete_user_timezone(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)

    add_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[1], user_username1, user_password1)
    add_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[2], user_username1, user_password1)

    r1 = delete_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[1], user_username1,
                                 user_password1)
    assert r1.status_code == httplib.OK

    user = get_me(flask_test_server, user_username1, user_password1)
    data = json.loads(user.data)
    assert data['timezones'][0]['timezone_name'] == pytz.all_timezones[2]


def test_delete_user_timezone_by_manager_permission_ok(flask_test_server):
    user_username1 = 'random1@email.com'
    user_password1 = 'hounds123'

    user_username2 = 'random2@email.com'
    user_password2 = 'hounds123'

    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username1, user_password1)
    sign_up(flask_test_server, "Daniil1", 'Archipov1', user_username2, user_password2)

    update_role_for_user(flask_test_server, user_username2, Role.USER_MANAGER.name)
    r1 = add_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[1], user_username2, user_password2)
    assert r1.status_code == httplib.OK

    r1 = delete_timezone_to_user(flask_test_server, user_username1, pytz.all_timezones[1], user_username2,
                                 user_password2)
    assert r1.status_code == httplib.OK

    user = get_me(flask_test_server, user_username1, user_password1)
    data = json.loads(user.data)
    assert len(data['timezones']) == 0


def sign_up(flask_test_server, first_name, last_name, username, password):
    sing_up_payload = {'first_name': first_name, 'last_name': last_name, 'email': username,
                       'password': password}
    return flask_test_server.put('/account/sign_up', data=json.dumps(dict(sing_up_payload)),
                                 content_type='application/json')


def get_me(flask_test_server, username, password):
    username_password = b64encode(b"{0}:{1}".format(username, password)).decode("ascii")
    return flask_test_server.get('/account/me', headers={'Authorization': 'Basic %s' % username_password})


def update_role_for_user(flask_test_server, username_to_update, role_to_set, admin_username='admin@example.com',
                         admin_password='Pa55word'):
    update_role_payload = {'operation_for_username': username_to_update,
                           'payload': {'new_role': role_to_set}}

    return flask_test_server.post('/role/user/update', data=json.dumps(dict(update_role_payload)),
                                  headers=generate_basic_auth_header(admin_username, admin_password),
                                  content_type='application/json')


def get_user_by_username(flask_test_server, username_to_get, auth_username='admin@example.com',
                         auth_password='Pa55word'):
    get_user_payload = {'operation_for_username': username_to_get}
    return flask_test_server.post('/account/user', data=json.dumps(dict(get_user_payload)),
                                 headers=generate_basic_auth_header(auth_username, auth_password),
                                 content_type='application/json')


def delete_user(flask_test_server, username_to_delete, auth_username, auth_password):
    del_payload = {'operation_for_username': username_to_delete}
    return flask_test_server.delete('/account/user/delete', data=json.dumps(dict(del_payload)),
                                    headers=generate_basic_auth_header(auth_username, auth_password),
                                    content_type='application/json')


def update_user_details(flask_test_server, username_to_update, new_first_name, new_last_name, new_email, new_password,
                        auth_username, auth_password):
    payload = {'operation_for_username': username_to_update,
               'payload': {'first_name': new_first_name, 'last_name': new_last_name, 'email': new_email,
                           'password': new_password}}

    return flask_test_server.post('/account/user/update',
                                  headers=generate_basic_auth_header(auth_username, auth_password),
                                  data=json.dumps(dict(payload)), content_type='application/json')


def add_timezone_to_user(flask_test_server, username_to_update, timezone_name, auth_username='admin@example.com',
                         auth_password='Pa55word'):
    payload = {'operation_for_username': username_to_update,
               'payload': {'timezone': timezone_name}}
    return flask_test_server.put('/timezone/user/add', headers=generate_basic_auth_header(auth_username, auth_password),
                                 data=json.dumps(dict(payload)), content_type='application/json')


def delete_timezone_to_user(flask_test_server, username_to_update, timezone_name, auth_username='admin@example.com',
                            auth_password='Pa55word'):
    payload = {'operation_for_username': username_to_update,
               'payload': {'timezone': timezone_name}}
    return flask_test_server.delete('/timezone/user/delete',
                                    headers=generate_basic_auth_header(auth_username, auth_password),
                                    data=json.dumps(dict(payload)), content_type='application/json')


def generate_basic_auth_header(username, password):
    return {'Authorization': 'Basic %s' % b64encode(b"{0}:{1}".format(username, password)).decode("ascii")}
