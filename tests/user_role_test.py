from app.domain.role import Role
from tests.test_utils import generate_and_register_random_user


def test_user_role_get(user_role_service):
    role = user_role_service.get_role(Role.USER_MANAGER.name)
    assert role.role_id == Role.USER_MANAGER.value


def test_update_user_role(account_service, user_role_service):
    u = generate_and_register_random_user(account_service)
    assert u.role.role_name == Role.USER.name
    user_role_service.update_user_role(u.user_id, Role.USER_MANAGER.value)
    update_user = account_service.get_user(u.user_id)
    assert update_user.role.role_name == Role.USER_MANAGER.name
