# !venv/bin/python
import pytest
import os
import tempfile

import pytz

from app.domain.role import Role
from app.repositories.user_role_sql_repository import UserRoleSqlRepository
from app.repositories.user_timezone_sql_repository import UserTimezoneSqlRepository
from app.services.user_account_service import UserAccountService
from app.repositories.user_sql_repository import UserSqlRepository
from app.services.user_role_service import UserRoleService
from app.services.user_timezone_service import UserTimezoneService

from sqlalchemy import create_engine
from app.db_base.base_sqlalchemy import Base, ForeignKeysListener

from app.app import server
from setup import get_db

handle, file_path = tempfile.mkstemp(".db", "clock_db_test")
server.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(file_path)


@pytest.fixture(scope="function")
def db_session():
    try:
        engine = create_engine(server.config['SQLALCHEMY_DATABASE_URI'], listeners=[ForeignKeysListener()], echo=False)
        Base.metadata.create_all(engine)
        db_session = get_db()
        db_seed(db_session)
        yield db_session
    finally:
        os.remove(file_path)


@pytest.fixture(scope="function")
def flask_test_server(db_session):
    s = server.test_client()
    return s


@pytest.fixture(scope="function")
def account_service(db_session):
    return UserAccountService(UserSqlRepository(db_session))


@pytest.fixture(scope="function")
def user_timezone_service(db_session):
    return UserTimezoneService(UserTimezoneSqlRepository(db_session))


@pytest.fixture(scope="function")
def user_role_service(db_session):
    return UserRoleService(UserRoleSqlRepository(db_session))


def db_seed(db_session):
    user_role_service = UserRoleService(UserRoleSqlRepository(db_session))
    user_role_service.add_role(Role.USER.value, Role.USER.name)
    user_role_service.add_role(Role.USER_MANAGER.value, Role.USER_MANAGER.name)
    user_role_service.add_role(Role.ADMIN.value, Role.ADMIN.name)

    user_timezone_service = UserTimezoneService(UserTimezoneSqlRepository(db_session))
    for i in range(0, 3):
        user_timezone_service.add_timezone(pytz.all_timezones[i])

    account_service = UserAccountService(UserSqlRepository(db_session))
    account_service.sign_up('Daniil', 'Archipov', 'admin@example.com', 'Pa55word', Role.ADMIN.value)
