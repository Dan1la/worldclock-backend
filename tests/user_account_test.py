import pytest
from sqlalchemy.orm.exc import NoResultFound

from app.domain.role import Role
from app.exceptions.user_already_registered import UserAlreadyRegistered
from app.utils.utils import validate_password
from test_utils import generate_random_user
from test_utils import generate_and_register_random_user



def test_is_email_registered(account_service):
    u = generate_random_user()
    account_service.sign_up(u.first_name, u.last_name, u.email, u.hashed_password, Role.USER.value)
    assert account_service.is_email_registered(u.email) == True
    assert account_service.is_email_registered('other@example.com') == False


def test_user_sign_up(account_service):
    u = generate_random_user()
    account_service.sign_up(u.first_name, u.last_name, u.email, u.hashed_password, Role.USER.value)
    assert account_service.is_email_registered(u.email) == True


def test_user_already_exist(account_service):
    with pytest.raises(UserAlreadyRegistered):
        u = generate_random_user()
        u2 = generate_random_user()
        account_service.sign_up(u.first_name, u.last_name, u.email, u.hashed_password, Role.USER.value)
        account_service.sign_up(u2.first_name, u2.last_name, u.email, u2.hashed_password, Role.USER.value)


def test_get_all_users(account_service):
    registered_users = [generate_and_register_random_user(account_service) for x in range(0, 3)]
    users = account_service.get_all_users()
    # All recently registered users + 1 admin user initialized on db start
    assert len(registered_users) + 1 == len(users)


def test_update_user_details(account_service, user_role_service):
    u = generate_and_register_random_user(account_service)
    account_service.update_user_details(u.user_id, 'Daniil', 'Arkhipov', 'test@gmail.com', '321312')
    u = account_service.get_user(u.user_id)
    assert u.first_name == 'Daniil' and u.last_name == 'Arkhipov' and u.email == 'test@gmail.com' and validate_password(
        '321312', u.hashed_password)


def test_get_user(account_service):
    u1 = generate_and_register_random_user(account_service)
    u2 = account_service.get_user(u1.user_id)
    assert u1.user_id == u2.user_id and u1.first_name == u2.first_name and u1.last_name == u2.last_name and u1.email == u2.email and u1.hashed_password == u2.hashed_password


def test_delete_user(account_service):
    with pytest.raises(NoResultFound):
        u = generate_random_user()
        account_service.delete_user(u.user_id)
        account_service.get_user(u.user_id)
