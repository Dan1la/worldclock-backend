import string
import random

from app.domain.role import Role
from app.models.models import User


def generate_and_register_random_user(account_service):
    name = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    last_name = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    email = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    password = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    return account_service.get_user(account_service.sign_up(name, last_name, email, password, Role.USER.value))


def generate_random_user():
    name = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    last_name = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    email = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    password = ''.join(random.choice(string.ascii_uppercase) for _ in range(10))
    return User(name, last_name, email, password, Role.USER.value)
