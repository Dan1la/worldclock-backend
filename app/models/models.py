from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.db_base.base_sqlalchemy import Base


class Timezone(Base):
    __tablename__ = 'timezone'
    timezone_id = Column(Integer, primary_key=True)
    name = Column(String(40), unique=True)


    def __init__(self, name, id=None):
        self.name = name
        if id is not None:
            self.timezone_id = id

    @property
    def serialize(self):
        return {
            'timezone_id': self.timezone_id,
            'timezone_name': self.name
        }

    def __repr__(self):
        return '<TimeZone %r %r>' % (self.timezone_id, self.name)


class UserTimezone(Base):
    __tablename__ = 'user_timezone'
    user_timezone_id = Column(Integer, autoincrement=True, unique=True)
    user_id = Column(Integer, ForeignKey('users.user_id', ondelete='CASCADE'), primary_key=True)
    timezone_id = Column(Integer, ForeignKey('timezone.timezone_id', ondelete='CASCADE'), primary_key=True)

    def __init__(self, user_id, timezone_id, id=None):
        self.user_id = user_id
        self.timezone_id = timezone_id
        if id is not None:
            self.user_timezone_id = id

    timezone = relationship(Timezone, backref="timezone")

    @property
    def serialize(self):
        return {
            'timezone_id': self.timezone_id,
            'timezone_name': self.timezone.name
        }

    def __repr__(self):
        return '<UserTimeZone %r %r %r>' % (self.user_timezone_id, self.user_id, self.timezone_id)


class Role(Base):
    __tablename__ = 'role'
    role_id = Column(Integer, primary_key=True)
    role_name = Column(String(32), unique=True)

    def __init__(self, role_name, id=None):
        self.role_name = role_name
        if id is not None:
            self.role_id = id


class User(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True)
    first_name = Column(String(64), index=False, unique=False)
    last_name = Column(String(64), index=False, unique=False)
    email = Column(String(120), index=True, unique=True)
    hashed_password = Column(String(120), index=False, unique=False)
    role_id = Column(Integer, ForeignKey('role.role_id'))

    def __init__(self, first_name, last_name, email, hashed_password, role_id, id=None):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.hashed_password = hashed_password
        self.role_id = role_id
        if id is not None:
            self.user_id = id

    timezones = relationship(UserTimezone, cascade="all,delete", backref="user_timezone")
    role = relationship(Role, backref="role")

    @property
    def serialize(self):
        return {
            'user_id': self.user_id,
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email,
            'role': self.role.role_name,
            'timezones': self.serialize_timezones
        }

    @property
    def serialize_timezones(self):
        return [timezone.serialize for timezone in self.timezones]

    def __repr__(self):
        return '<User %r %r %r>' % (self.first_name, self.last_name, self.email)
