from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.interfaces import PoolListener

Base = declarative_base()

class ForeignKeysListener(PoolListener):
    def connect(self, dbapi_con, con_record):
        db_cursor = dbapi_con.execute('pragma foreign_keys=ON')
