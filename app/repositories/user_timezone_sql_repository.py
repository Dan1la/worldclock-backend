from sqlalchemy.orm.strategy_options import joinedload

from app.models.models import UserTimezone
from app.models.models import Timezone


class UserTimezoneSqlRepository:
    def __init__(self, session):
        self.session = session

    def get_timezone(self, timezone_id):
        return self.session.query(Timezone).filter(Timezone.timezone_id == timezone_id).one()

    def get_timezone_by_name(self, timezone_name):
        return self.session.query(Timezone).filter(Timezone.name == timezone_name).one()

    def add_timezone(self, timezone):
        t = Timezone(timezone)
        self.session.add(t)
        self.session.flush()
        self.session.refresh(t)
        self.session.commit()
        return t.timezone_id

    def delete_timezone(self, timezone_id):
        t = self.session.query(Timezone).filter(Timezone.timezone_id == timezone_id).one()
        self.session.delete(t)
        self.session.commit()

    def add_user_timezone(self, user_id, timezone_id):
        ut = UserTimezone(user_id, timezone_id)
        self.session.add(ut)
        self.session.flush()
        self.session.refresh(ut)
        self.session.commit()
        return ut.user_timezone_id

    def delete_user_timezone(self, user_id, timezone_id):
        t = self.session.query(UserTimezone).filter(UserTimezone.user_id == user_id).filter(UserTimezone.timezone_id == timezone_id).one()
        self.session.delete(t)
        self.session.commit()
