from app.models.models import User


class UserSqlRepository:
    def __init__(self, session):
        self.session = session

    def add_user(self,first_name, last_name, email, hashed_password, role_id):
        u = User(first_name, last_name, email, hashed_password, role_id)
        self.session.add(u)
        self.session.flush()
        self.session.refresh(u)
        self.session.commit()
        return u.user_id

    def is_email_already_used(self, email):
        return len(self.session.query(User).filter(User.email == email).all()) > 0

    def get_user_by_email(self, email):
        return self.session.query(User).filter(User.email == email).one()

    def update_user_details(self, user_id, first_name, last_name, email, hashed_password):
        self.session.query(User).filter_by(user_id=user_id).update({"first_name": first_name, "last_name" : last_name, "email" : email, "hashed_password" : hashed_password})
        self.session.commit()

    def update_user_details_without_password(self, user_id, first_name, last_name, email):
        self.session.query(User).filter_by(user_id=user_id).update({"first_name": first_name, "last_name" : last_name, "email" : email})
        self.session.commit()


    def get_user(self, user_id):
        return self.session.query(User).filter_by(user_id=user_id).one()

    def get_all_users(self):
        return self.session.query(User).all()

    def delete_user(self, user_id):
        u = self.get_user(user_id)
        self.session.delete(u)
        self.session.commit()