from app.models.models import Role, User


class UserRoleSqlRepository:
    def __init__(self, session):
        self.session = session

    def add_role(self, role_id, role_name):
        r = Role(role_name, role_id)
        self.session.add(r)
        self.session.flush()
        self.session.refresh(r)
        self.session.commit()
        return r.role_id

    def get_role(self, role_name):
        return self.session.query(Role).filter(Role.role_name == role_name).one()

    def update_user_role(self, user_id, role_id):
        self.session.query(User).filter_by(user_id=user_id).update({'role_id': role_id})
        self.session.commit()
