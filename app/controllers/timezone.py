import pytz
from flask import g
from flask import request, jsonify

from app.app import server
from app.controllers.request_validator import delete_timezone_schema, add_timezone_schema
from app.domain.role import Role
from app.exceptions.invalid_timezone import InvalidTimezone
from app.filters.authentication_filter import requires_auth
from app.filters.permission_filter import validate_permission
from app.filters.request_validation_filter import validate_request
from app.filters.roles_filter import required_roles
from app.repositories.user_timezone_sql_repository import UserTimezoneSqlRepository
from app.services.user_timezone_service import UserTimezoneService
from setup import get_db



@server.route('/timezone/all', methods=['GET'])
@requires_auth
@required_roles(Role.USER.name, Role.USER_MANAGER.name, Role.ADMIN.name)
def get_all_timezones():
    user_timezone_repository = UserTimezoneService(UserTimezoneSqlRepository(get_db()))
    timezones = user_timezone_repository.get_all_timezones()
    return jsonify(timezones)


@server.route('/timezone/user/add', methods=['PUT'])
@validate_request(add_timezone_schema)
@requires_auth
@required_roles(Role.USER.name, Role.USER_MANAGER.name, Role.ADMIN.name)
@validate_permission
def add_timezone():
    data = request.get_json(force=True)['payload']
    if data['timezone'] not in pytz.all_timezones:
        raise InvalidTimezone('Invalid timezone supplied: {}'.format(data['timezone']))
    user_timezone_repository = UserTimezoneService(UserTimezoneSqlRepository(get_db()))
    user_timezone_repository.add_user_timezone_by_name(g.user_to_update.user_id, data['timezone'])
    return jsonify("OK.")


@server.route('/timezone/user/delete', methods=['DELETE'])
@validate_request(delete_timezone_schema)
@requires_auth
@required_roles(Role.USER.name, Role.USER_MANAGER.name, Role.ADMIN.name)
@validate_permission
def delete_timezone():
    data = request.get_json(force=True)['payload']
    user_timezone_repository = UserTimezoneService(UserTimezoneSqlRepository(get_db()))
    user_timezone_repository.delete_user_timezone_by_name(g.user_to_update.user_id, data['timezone'])
    return jsonify("OK.")
