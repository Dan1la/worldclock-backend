from app.app import server
from flask import jsonify


@server.route('/')
@server.route('/index', methods=['GET'])
def index():
    return jsonify('Welcome to world clock API.')
