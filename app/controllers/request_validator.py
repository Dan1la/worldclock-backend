from jsonschema import SchemaError
from jsonschema import ValidationError
from jsonschema import validate

from app.exceptions.request_validation_error import RequestValidationError

sign_up_schema = {
    "type": "object",
    "properties": {
        "first_name": {"type": "string"},
        "last_name": {"type": "string"},
        "email": {"type": "string"},
        "password": {"type": "string"}
    },
    "required": ["first_name", "last_name", "email", "password"],
    "additionalProperties": False

}

get_user_schema = {
    "type": "object",
    "properties": {
        "operation_for_username": {"type": "string"}
    },
    "required": ["operation_for_username"],
    "additionalProperties": False
}

update_details_schema = {
    "type": "object",
    "properties": {
        "operation_for_username": {"type": "string"},
        "payload": {
            "type": "object",
            "properties": {
                "first_name": {"type": "string"},
                "last_name": {"type": "string"},
                "email": {"type": "string"},
                "password": {"type": "string"}
            },
            "required": ["first_name", "last_name", "email"],
            "additionalProperties": False
        }
    },
    "required": ["operation_for_username", "payload"],
    "additionalProperties": False
}

delete_user_schema = {
    "type": "object",
    "properties": {
        "operation_for_username": {"type": "string"}
    },
    "required": ["operation_for_username"],
    "additionalProperties": False
}

update_role_schema = {
    "type": "object",
    "properties": {
        "operation_for_username": {"type": "string"},
        "payload": {
            "type": "object",
            "properties": {
                "new_role": {"type": "string"}
            },
            "required": ["new_role"],
            "additionalProperties": False
        }
    },
    "required": ["operation_for_username", "payload"],
    "additionalProperties": False
}

add_timezone_schema = {
    "type": "object",
    "properties": {
        "operation_for_username": {"type": "string"},
        "payload": {
            "type": "object",
            "properties": {
                "timezone": {"type": "string"}
            },
            "required": ["timezone"],
            "additionalProperties": False
        }
    },
    "required": ["operation_for_username", "payload"],
    "additionalProperties": False
}

delete_timezone_schema = {
    "type": "object",
    "properties": {
        "operation_for_username": {"type": "string"},
        "payload": {
            "type": "object",
            "properties": {
                "timezone": {"type": "string"}
            },
            "required": ["timezone"],
            "additionalProperties": False
        }
    },
    "required": ["operation_for_username", "payload"],
    "additionalProperties": False
}


def validate_json(json, schema):
    try:
        validate(json, schema)
    except ValidationError as e:
        raise RequestValidationError(e.message)
    except SchemaError as e2:
        raise RequestValidationError(e2.message)
