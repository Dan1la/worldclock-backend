from flask import request, jsonify

from app.app import server
from app.controllers.request_validator import update_role_schema
from app.domain.role import Role
from app.filters.authentication_filter import requires_auth
from app.filters.request_validation_filter import validate_request
from app.filters.roles_filter import required_roles
from app.repositories.user_role_sql_repository import UserRoleSqlRepository
from app.repositories.user_sql_repository import UserSqlRepository
from app.services.user_account_service import UserAccountService
from app.services.user_role_service import UserRoleService
from setup import get_db


@server.route('/role/user/update', methods=['POST'])
@validate_request(update_role_schema)
@requires_auth
@required_roles(Role.ADMIN.name)
def role_update():
    account_service = UserAccountService(UserSqlRepository(get_db()))
    user_role_service = UserRoleService(UserRoleSqlRepository(get_db()))
    data = request.get_json(force=True)
    user_to_update = account_service.get_user_by_email(data['operation_for_username'])
    role = user_role_service.get_role(data['payload']['new_role'])
    user_role_service.update_user_role(user_to_update.user_id, role.role_id)
    return jsonify("OK.")