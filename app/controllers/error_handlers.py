from flask.json import jsonify

from app.app import server
from app.exceptions.invalid_timezone import InvalidTimezone
from app.exceptions.request_validation_error import RequestValidationError
from app.exceptions.user_already_registered import UserAlreadyRegistered
from app.exceptions.not_allowed import NotAllowed
from app.exceptions.not_found import NotFound


@server.errorhandler(NotAllowed)
@server.errorhandler(NotFound)
@server.errorhandler(UserAlreadyRegistered)
@server.errorhandler(RequestValidationError)
@server.errorhandler(InvalidTimezone)
def not_allowed(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response

@server.errorhandler(404)
def not_found(error):
    return error


@server.errorhandler(500)
def internal_error(error):
    return jsonify('Unexpected error occurred')
