from flask import jsonify, request
from flask_restful import reqparse

from app.exceptions.user_already_registered import UserAlreadyRegistered
from app.filters.request_validation_filter import validate_request
from app.services.exceptions import UserAlreadyRegisteredError

reqparser = reqparse.RequestParser()

from app.app import server
from app.exceptions.not_allowed import NotAllowed
from app.exceptions.not_found import NotFound
from app.filters.permission_filter import validate_permission
from app.filters.roles_filter import required_roles
from app.repositories.user_sql_repository import UserSqlRepository
from app.services.user_account_service import UserAccountService
from setup import get_db

from app.domain.role import Role
from app.filters.authentication_filter import requires_auth

from flask import g
from request_validator import *

@server.route('/account/sign_up', methods=['PUT'])
@validate_request(sign_up_schema)
def user_sign_up():
    data = request.get_json(force=True)
    account_service = UserAccountService(UserSqlRepository(get_db()))
    return jsonify(account_service.sign_up(data['first_name'], data['last_name'], data['email'], data['password'],
                                           Role.USER.value))


@server.route('/account/user/login', methods=['GET'])
@requires_auth
def user_login():
    return jsonify(g.user.serialize)


@server.route('/account/me', methods=['GET'])
@requires_auth
@required_roles(Role.USER.name, Role.USER_MANAGER.name, Role.ADMIN.name)
def get_me():
    return jsonify(g.user.serialize)


@server.route('/account/get/user', methods=['GET'])
@requires_auth
@required_roles(Role.USER.name, Role.USER_MANAGER.name, Role.ADMIN.name)
def get_user_get():
    username =  request.args.get('user')
    account_service = UserAccountService(UserSqlRepository(get_db()))
    user = account_service.get_user_by_email(username)
    return jsonify(user.serialize)


@server.route('/account/user', methods=['POST'])
@validate_request(get_user_schema)
@requires_auth
@required_roles(Role.USER.name, Role.USER_MANAGER.name, Role.ADMIN.name)
def get_user():
    username = request.get_json(force=True)['operation_for_username']
    account_service = UserAccountService(UserSqlRepository(get_db()))
    user = account_service.get_user_by_email(username)
    return jsonify(user.serialize)


@server.route('/account/user/all', methods=['GET'])
@requires_auth
@required_roles(Role.USER_MANAGER.name, Role.ADMIN.name)
def get_all_users():
    account_service = UserAccountService(UserSqlRepository(get_db()))
    users = account_service.get_all_users()
    return jsonify([user.serialize for user in users])


@server.route('/account/user/update', methods=['POST'])
@validate_request(update_details_schema)
@requires_auth
@required_roles(Role.USER.name, Role.USER_MANAGER.name, Role.ADMIN.name)
@validate_permission
def user_update():
    data = request.get_json(force=True)['payload']
    account_service = UserAccountService(UserSqlRepository(get_db()))
    password = None
    if 'password' in data and len(data['password']) > 0:
        password = data['password']

    account_service.update_user_details(g.user_to_update.user_id, data['first_name'], data['last_name'], data['email'],
                                        password)
    return jsonify("OK.")


@server.route('/account/user/delete', methods=['DELETE'])
@validate_request(delete_user_schema)
@requires_auth
@required_roles(Role.USER.name, Role.USER_MANAGER.name, Role.ADMIN.name)
@validate_permission
def delete_user():
    data = request.get_json(force=True)
    account_service = UserAccountService(UserSqlRepository(get_db()))
    user = account_service.get_user_by_email(data['operation_for_username'])
    account_service.delete_user(user.user_id)
    return jsonify("OK.")

