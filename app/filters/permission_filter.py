from functools import wraps
from flask import g, request

from app.domain.role import Role
from app.exceptions.not_allowed import NotAllowed
from app.repositories.user_sql_repository import UserSqlRepository
from app.services.user_account_service import UserAccountService
from setup import get_db


def validate_permission(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        check_permissions()
        return f(*args, **kwargs)

    return decorated


def check_permissions():
    current_user = get_current_user()

    data = request.get_json(force=True)
    username_to_update = data['operation_for_username']
    account_service = UserAccountService(UserSqlRepository(get_db()))
    user_to_update = account_service.get_user_by_email(username_to_update)
    g.user_to_update = user_to_update

    if current_user.role.role_name == Role.ADMIN.name:
        return True
    if current_user.email == user_to_update.email:
        return True

    if current_user.role.role_name == Role.USER_MANAGER.name and user_to_update.role.role_name == Role.USER.name:
        return True

    raise NotAllowed(
        'You are not allowed to execute current operation. Not enough permission for role:{}.'.format(
            current_user.role.role_name))


def get_current_user():
    return g.user
