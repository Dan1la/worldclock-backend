from functools import wraps
from flask import request
from app.controllers.request_validator import validate_json, update_details_schema


def validate_request(schema):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            data = request.get_json(force=True)
            validate_json(data, schema)
            return f(*args, **kwargs)
        return wrapped
    return wrapper
