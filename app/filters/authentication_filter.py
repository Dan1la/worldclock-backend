from functools import wraps

import bcrypt
from flask import g
from flask import request, Response

from app.repositories.user_sql_repository import UserSqlRepository
from app.services.user_account_service import UserAccountService
from setup import get_db


def check_auth(username, password):
    account_service = UserAccountService(UserSqlRepository(get_db()))
    user = account_service.get_user_by_email(username)
    is_authenticated = bcrypt.hashpw(password.encode('utf-8'), user.hashed_password.encode('utf-8')) == user.hashed_password
    if is_authenticated:
        g.user = user
        return True

    return False


def unable_to_authenticate():
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if auth is None or not check_auth(auth.username, auth.password):
            return unable_to_authenticate()
        return f(*args, **kwargs)

    return decorated
