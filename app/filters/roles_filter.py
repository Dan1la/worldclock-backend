from functools import wraps
from flask import g
from app.exceptions.not_allowed import NotAllowed


def required_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            user_role = get_current_user_role()
            if user_role not in roles:
                raise NotAllowed('You are not allowed to execute current operation. Roles required: {}. Your role: {}'.format(roles, user_role))
            return f(*args, **kwargs)

        return wrapped

    return wrapper


def get_current_user_role():
    return g.user.role.role_name