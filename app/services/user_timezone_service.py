import pytz
from sqlalchemy.orm.exc import NoResultFound

from app.exceptions.not_found import NotFound


class UserTimezoneService:
    def __init__(self, user_timezone_sql_repository):
        self.user_timezone_sql_repository = user_timezone_sql_repository

    def add_user_timezone(self, user_id, timezone_id):
        return self.user_timezone_sql_repository.add_user_timezone(user_id, timezone_id)

    def add_user_timezone_by_name(self, user_id, timezone_name):
        timezone = self.get_timezone_by_name(timezone_name)
        return self.user_timezone_sql_repository.add_user_timezone(user_id, timezone.timezone_id)

    def delete_timezone(self, timezone_id):
        self.user_timezone_sql_repository.delete_timezone(timezone_id)

    def add_timezone(self, timezone):
        return self.user_timezone_sql_repository.add_timezone(timezone)

    def get_timezone(self, timezone_id):
        return self.user_timezone_sql_repository.get_timezone(timezone_id)

    def get_timezone_id(self, timezone_name):
        return self.user_timezone_sql_repository.get_timezone_by_name(timezone_name)

    def get_timezone_by_name(self, timezone_name):
        return self.user_timezone_sql_repository.get_timezone_by_name(timezone_name)

    def delete_user_timezone(self, user_id, timezone_id):
        self.user_timezone_sql_repository.delete_user_timezone(user_id, timezone_id)

    def delete_user_timezone_by_name(self, user_id, timezone_name):
        try:
            timezone = self.get_timezone_by_name(timezone_name)
            self.user_timezone_sql_repository.delete_user_timezone(user_id, timezone.timezone_id)
        except NoResultFound:
            raise NotFound('No timezone {} found for user_id: {}'.format(timezone_name, user_id))

    def get_all_timezones(self):
        return pytz.all_timezones