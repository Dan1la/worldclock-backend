import bcrypt
from sqlalchemy.orm.exc import NoResultFound

from app.exceptions.user_already_registered import UserAlreadyRegistered
from app.exceptions.not_found import NotFound
from app.services.exceptions import UserAlreadyRegisteredError
from app.utils.utils import hash_password


class UserAccountService:
    def __init__(self, user_sql_repository):
        self.user_sql_repository = user_sql_repository

    def sign_up(self, first_name, last_name, email, password, role_id):
        if not self.user_sql_repository.is_email_already_used(email):
            return self.user_sql_repository.add_user(first_name, last_name, email, hash_password(password), role_id)
        else:
            raise UserAlreadyRegistered('Failed to Sign Up user. Email already in use: {}'.format(email))

    def is_email_registered(self, email):
        return self.user_sql_repository.is_email_already_used(email)

    def update_user_details(self, user_id, new_first_name, new_last_name, new_user_email, new_password):
        if new_password is None or len(new_password) == 0:
            self.user_sql_repository.update_user_details_without_password(user_id, new_first_name, new_last_name, new_user_email)
        else:
            self.user_sql_repository.update_user_details(user_id, new_first_name, new_last_name, new_user_email,
                                                     hash_password(new_password))

    def get_user(self, user_id):
        return self.user_sql_repository.get_user(user_id)

    def get_user_by_email(self, email):
        try:
            return self.user_sql_repository.get_user_by_email(email)
        except NoResultFound:
            raise NotFound('No user found for given email: {}'.format(email))

    def get_all_users(self):
        return self.user_sql_repository.get_all_users()

    def delete_user(self, user_id):
        self.user_sql_repository.delete_user(user_id)
