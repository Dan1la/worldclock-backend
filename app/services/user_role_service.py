class UserRoleService:
    def __init__(self, user_role_sql_repository):
        self.user_role_sql_repository = user_role_sql_repository

    def add_role(self, role_id, role_name):
        return self.user_role_sql_repository.add_role(role_id, role_name)

    def get_role(self, role_name):
        return self.user_role_sql_repository.get_role(role_name)

    def update_user_role(self, user_id, role_id):
        self.user_role_sql_repository.update_user_role(user_id, role_id)